### Requirements to build project
- Node.js > 8.x
- npm > 5.x

### How to run
- npm install
- npm start
- type "npm run build" to build project into dist folder

### Demo
[Live Demo](https://movie-viewer.azurewebsites.net)

### Repository
[Bitbucket Repository](https://bitbucket.org/manosxviii/movie-viewer/src/master/)

