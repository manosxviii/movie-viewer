import { GbResources } from './gbResources';

export class AppConfig{

  public static _lang: string;

  public static AppConfig = {
    BaseURL : "",
    posterThumbUrl: "https://image.tmdb.org/t/p/w300_and_h450_bestv2",
    profileThumbUrl: "https://image.tmdb.org/t/p/w45",
    posterUrl: "https://image.tmdb.org/t/p/w500",
    backPosterUrl: "https://image.tmdb.org/t/p/w1400_and_h450_face",
    endpoint: "https://api.themoviedb.org/4/discover/movie?sort_by=popularity.desc&api_key=9198fa6d9a9713bc6b03ee9582525917",
    castEndpoint: "https://api.themoviedb.org/3/movie/{id}/credits?api_key=9198fa6d9a9713bc6b03ee9582525917"
  }

  public static getLanguageRes(){
    return GbResources;
  }
}

