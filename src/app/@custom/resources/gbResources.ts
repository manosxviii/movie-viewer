export class GbResources {

  public static HttpMessages = {
    generalError: "Error.",
  }

  public static Movie = {
    overview: "Overview",
    vote_average: "User Score",
    vote_count: "Votes"
  }
}
