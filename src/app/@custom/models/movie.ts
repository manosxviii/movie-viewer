export class Movie {
  id: number;
  vote_count: number;
  video: boolean;
  vote_average: number;
  title: string;
  popularity: number;
  poster_path: string;
  original_language: string;
  original_title: string;
  genre_ids: number[];
  backdrop_path: string;
  adult: false;
  overview: string;
  release_date: Date;
  videos: Video[];
  cast: Cast[];
}

export class Video{
  key: string;
  site: string;
}

export class Cast{
  cast_id: number;
  character: string;
  credit_id: string;
  gender: number;
  id: number;
  name: string;
  order: number;
  profile_path: string;
}
