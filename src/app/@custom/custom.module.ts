import { NgModule, ModuleWithProviders } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../@theme/theme.module';

import { NgCircleProgressModule } from 'ng-circle-progress';

import {TableModule} from 'primeng/table';

import {
  ContextMenuModule,
  DataScrollerModule,
  GrowlModule,
  ScrollPanelModule,
  LightboxModule,
  MultiSelectModule,
  SharedModule,
  OverlayPanelModule,
  DialogModule,
  InputTextModule,
  TabViewModule,
} from 'primeng/primeng';

import {
  HttpService,
  growlService,
  GenericResolve,
  ImageService
} from './services';

import {
  ResourcesService
} from './resources';

import {
  GrowlComponent,
  DetailViewComponent,
  SideViewComponent
} from './components'

const MODULES = [
  ContextMenuModule,
  DataScrollerModule,
  GrowlModule,
  ScrollPanelModule,
  LightboxModule,
  MultiSelectModule,
  NgCircleProgressModule.forRoot(),
  SharedModule,
  OverlayPanelModule,
  ReactiveFormsModule,
  DialogModule,
  InputTextModule,
  TabViewModule,
  TableModule
]

const SERVICES = [
  GenericResolve,
  ImageService,
  HttpService,
  growlService,
  ResourcesService
];

const COMPONENTS = [
  DetailViewComponent,
  GrowlComponent,
  SideViewComponent
];

const MODELS = [
];

@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    ...MODULES
  ],
  exports: [
    ...COMPONENTS
  ],

  declarations: [
    ...COMPONENTS
  ],
  providers: [
    // ...SERVICES,
  ],
})
export class CustomModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: CustomModule,
      providers: [
        ...SERVICES,
        ...MODELS
      ],
    };
  }
}
