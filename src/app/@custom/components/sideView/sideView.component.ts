import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Movie } from "../../models";
import { HttpService } from "../../services";
import { ResourcesService } from "../../resources";

@Component({
  selector: 'ngx-sideView',
  styleUrls: ['./sideView.component.scss'],
  templateUrl: './sideView.component.html',
})

export class SideViewComponent {

  @Input() items: Movie[];
  @Output() onSelected = new EventEmitter<any>();
  totalResults: number = 40
  url: string;
  selectedItem: Movie;
  loading: boolean = false;
  page: number = 1;

  constructor(
    private _httpService: HttpService,
    private _resources: ResourcesService
  ) {
    this.url = this._resources.getAppConfig().endpoint;
  }

  onRowSelect(event) {
    this.onSelected.emit(this.selectedItem);
  }

  loadDataOnScroll(event) {
    this.page++;
    this._httpService.httpGet(this.url + '&page=' + this.page)
      .subscribe(res => {
        this.items = this.items.concat(res.results);
        this.totalResults = this.totalResults + 22;
      })
  }

  getScrollHeight() {
    let windowWidth = window.screen.width;
    let windowHeight = window.screen.height;
    if (windowWidth > 767) {
      return "700px";
    }
    else {
      return "300px"
    }
  }
}
