import { Component, Input, ViewEncapsulation } from "@angular/core";
import { Movie } from "../../models";
import { ResourcesService } from "../../resources";
import { Cast } from "../../models/movie";

@Component({
  selector: 'ngx-detailView',
  styleUrls: ['./detailView.component.scss'],
  templateUrl: './detailView.component.html',
  encapsulation: ViewEncapsulation.None
})

export class DetailViewComponent {

  @Input() model: Movie;
  resources: any;
  images: any[];
  videos: any[];
  appconfig: any;

  constructor(private _resources: ResourcesService) {
    this.resources = _resources.getModelResources('Movie');
    this.appconfig = _resources.getAppConfig();
  }

  ngOnChanges() {
    this.images = [];

    if (this.model.poster_path != null) {
      let image = { source: this.appconfig.posterUrl + this.model.poster_path, thumbnail: this.appconfig.posterThumbUrl + this.model.poster_path }
      this.images.push(image);
    }
  }

  getProfilePicUrl(input: Cast) {
    if (input.profile_path == null) {
      return 'assets/images/profile.png';
    }

    let ret = this.appconfig.profileThumbUrl + input.profile_path;
    return ret;
  }

  displayInit() {
    let selector = document.querySelector('p-lightbox img');
    if (selector != null) {
      document.querySelector('p-lightbox img').classList.add("img-fluid");
    }
  }

  ngAfterViewChecked() {
    this.displayInit();
  }
}
