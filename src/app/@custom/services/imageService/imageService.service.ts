import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ImageService {
  private _image = new Subject<string>();
  image$ = this._image.asObservable();

  constructor(){
  }

  setImage(input:string){
    this._image.next(input);
  }
}
