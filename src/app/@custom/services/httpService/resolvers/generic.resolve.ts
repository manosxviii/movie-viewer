import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { HttpService } from '../httpService.service';

@Injectable()
export class GenericResolve implements Resolve<any> {

    baseUrl: string;
    constructor(
        private _httpService: HttpService,
    ) {
    }

    resolve(route: ActivatedRouteSnapshot) {

        let row = [];
        let id = "";

        Object.keys(route.data).forEach(e => {
            row.push(route.data[e]);

        })

        let path: string = row[0];
        delete row[0];

        route.data = row;

        // better way, replace text.. TODO make it for multiple keys
        if (path.indexOf('{id}') > -1) {
            path = path.replace('{id}', route.params['id']);
        }

        return this._httpService.httpGet(path);
    }
}
