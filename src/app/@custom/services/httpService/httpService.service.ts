import { Injectable, ElementRef, Injector } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Http, Response, URLSearchParams, Headers } from '@angular/http';
import { growlService } from '../growlService';

@Injectable()
export class HttpService {

  injector: Injector;
  headers: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  constructor(
    private http: HttpClient,
    private _growlService: growlService,
    injector: Injector,
  ) { }

  JsonpHttpGet(con: string, params?: URLSearchParams): Observable<any> {
    return this.http.jsonp(con, 'callback')
      .catch(err => this._growlService.httpError(err));
  }

  httpGet(con: string, responseType?:any, params?: URLSearchParams): Observable<any> {
    return this.http.get(con, responseType)
      .catch(err => this._growlService.httpError(err));
  }
}
