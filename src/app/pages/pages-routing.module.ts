import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';
import { MoviesComponent } from './movies/movies.component';
import { GenericResolve } from '../@custom/services/httpService';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'home',
    component: HomeComponent,
  },{
    path: 'most-popular-movies',
    component: MoviesComponent,
    resolve: {
      models: GenericResolve
    },
    data: ['https://api.themoviedb.org/4/discover/movie?sort_by=popularity.desc&api_key=9198fa6d9a9713bc6b03ee9582525917']
  },
  {
    path: '',
    redirectTo: 'most-popular-movies',
    pathMatch: 'full',
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
