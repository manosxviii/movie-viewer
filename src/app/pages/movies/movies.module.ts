import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { MoviesComponent } from './movies.component';
import { CustomModule } from '../../@custom/custom.module';


@NgModule({
  imports: [
    ThemeModule,
    CustomModule
  ],
  declarations: [
    MoviesComponent,
  ],
})
export class MoviesModule { }
