import { Component } from '@angular/core';
import { Movie } from '../../@custom/models';
import { ActivatedRoute } from '@angular/router';
import { HttpService, ImageService } from '../../@custom/services';
import { ResourcesService } from '../../@custom/resources';

@Component({
  selector: 'ngx-movies',
  styleUrls: ['./movies.component.scss'],
  templateUrl: './movies.component.html',
})
export class MoviesComponent {

  models: Movie[];
  totalResults: number;
  selectedModel: Movie;
  appConfig: any;

  constructor(
    private activeRoute: ActivatedRoute,
    private _httpService: HttpService,
    private imageService: ImageService,
    private _resources: ResourcesService
  ) {
    this.models = this.activeRoute.snapshot.data['models'].results;
    this.selectedModel = this.models[0];
    this.totalResults = this.activeRoute.snapshot.data['models'].total_results;
    this.appConfig = this._resources.getAppConfig();
  }

  selected(event) {
    this.selectedModel = event;
    this.initDetails();
  }

  ngOnInit() {
    this.initDetails();
  }

  initDetails(): any {
    this.imageService.setImage('');

    if (this.selectedModel.backdrop_path != null) {
      let imageUrl = this.appConfig.backPosterUrl + this.selectedModel.backdrop_path;

      let responseType = { responseType: 'blob' }
      this._httpService.httpGet(imageUrl, responseType)
        .subscribe(res => {
          this.imageService.setImage(imageUrl);
        })
    }

    this._httpService.httpGet((this.appConfig.castEndpoint as string).replace("{id}", this.selectedModel.id.toString()))
      .subscribe(res => {
        this.selectedModel.cast = res.cast;
      })
  }
}
